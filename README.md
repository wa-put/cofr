## Create python virtual environment in the working directory:

https://docs.python.org/3/library/venv.html

```path_to_working_directory\python -m venv venv```

or 

```path_to_working_directory/python3 -m venv venv```

## Activate virtual environment:

- Windows (cmd.exe):
```path_to_working_directory\venv\Scripts\activate.bat```
- Linux (bash/zsh):
```path_to_working_directory/source ./venv/bin/activate```

## Clone the repository:

```path_to_working_directory/git clone https://bitbucket.org/wa-put/cofr.git```

## Install dependencies:

```path_to_working_directory/cofr/pip install -r requirements.txt```

## Start jupyter:

```path_to_working_directory/cofr/jupyter notebook```

For presentation press ```Alt-R``` (https://rise.readthedocs.io/)